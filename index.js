module.exports = function adapterGenerator(collection) {
  return function adapterConstructor() {
    
    // (pointer: any, callback: (error?, results?: [any])
    this.cursorFetch = function(pointer, callback) {
      callback(undefined, collection.find(pointer));
    };
    
    // (pointer: any, handler: (error?, results?: [any], callback: ())
    this.cursorEach = function(pointer, handler, callback) {
      this.cursorFetch(pointer, function(error, results) {
        if (error) callback(error);
        else {
          for (var r in results) {
            handler(undefined, results[r]);
          }
          callback();
        }
      });
    };
    
    // (pointer: any, callback: (error?, count?: Number)
    this.cursorCount = function(pointer, callback) {
      this.cursorFetch(pointer, function(error, results) {
        callback(error, results?results.length:0);
      });
    };
    
    // (pointer: any, begin: Number, length: Number, callback: (error?, results?: [any])
    this.cursorSlice = function(pointer, begin, length, callback) {
      this.cursorFetch(pointer, function(error, _results) {
        var results;
        if (_results) results = _results.slice(begin, begin+length);
        callback(error, results);
      });
    };
    
    // (pointer: any, callback(error?, result? any)
    this.cursorFirst = function(pointer, callback) {
      callback(undefined, collection.findOne(pointer));
    };
    
    return this;
  };
};