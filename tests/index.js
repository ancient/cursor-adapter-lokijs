var Loki = require('lokijs');
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Adapters = require('ancient-adapters').Adapters;
var Cursor = require('ancient-cursor').Cursor;
var AncientCursorAdapterLokiJS = require('../index.js');
var AncientRefsAdapterLokiJS = require('ancient-refs-adapter-lokijs');
var TestAncientCursor = require('ancient-cursor/tests/test.js');

describe('ancient-cursor-adapter-lokijs', function() {
  var db = new Loki(this.title);
  var collection = db.addCollection(this.title);
  collection.insert({});
  collection.insert({});
  collection.insert({});
  collection.insert({});
  collection.insert({});
  
  var adapters = new Adapters();
  
  var adapterTest = {};
  adapterTest = AncientCursorAdapterLokiJS(collection).call(adapterTest);
  adapterTest = AncientRefsAdapterLokiJS(collection).call(adapterTest);
  adapters.add('test', adapterTest);
  
  var documentsFactory = new DocumentsFactory(adapters);
  var refs = new Refs(documentsFactory);
  var cursor = new Cursor(refs, 'test', { $and: [ { $loki: { $lte: 3 } }, { $loki: { $gte: 1 } } ] });
  
  TestAncientCursor(cursor, [
    collection.get(1),
    collection.get(2),
    collection.get(3)
  ], true, true);
});