# CursorAdapterLokiJS^0.0.1 ^[wiki](https://gitlab.com/ancient/cursor-adapter-lokijs/wikis/home)

Adapter based on [LokiJS](http://lokijs.org/#/) for [ancient-cursor](https://gitlab.com/ancient/cursor) package.

## Example

```js
var loki = require('lokijs');

var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').Factory;
var Cursor = require('ancient-cursor').Cursor;
var AncientRefsAdapterLokiJS = require('ancient-refs-adapter-lokijs');
var AncientCursorAdapterLokiJS = require('ancient-cursor-adapter-lokijs');

var db = new loki('db');
var posts = db.addCollection('posts');

posts.insert({});
// { $loki: 1 }
posts.insert({});
// { $loki: 2 }
posts.insert({});
// { $loki: 3 }
posts.insert({});
// { $loki: 4 }
posts.insert({});
// { $loki: 5 }

var adapters = new Adapters();
var adaterPosts = {};
AncientRefsAdapterLokiJS(posts).call(adapterPosts);
AncientCursorAdapterLokiJS(posts).call(adapterPosts);
adapters.add('posts', adapterPosts);
var documentsFactory = new DocumentsFactory(adapters);
var refs = new Refs(documentsFactory);
var cursor = new Cursor(refs, 'posts', { $and: [ { $loki: { $lte: 3 } }, { $loki: { $gte: 1 } } ] });

cursor.fetch();
// [
// Document { data: { $loki: 1 } },
// Document { data: { $loki: 2 } },
// Document { data: { $loki: 3 } }
// ]

cursor.each(function(document) {
  console.log(document);
}, function() {
  console.log('done');
});
// Document { data: { $loki: 1 } }
// Document { data: { $loki: 2 } }
// Document { data: { $loki: 3 } }
// done

cursor.count();
// 2

cursor.slice(1, 1);
// [
// Document { data: { $loki: 1 } }
// ]

cursor.first();
// Document { data: { $loki: 1 } }
```

## Versions

### 0.0.1
* Add cursorFirst to adapter